# UBER Simulator API

This project is developed to implement two different APIs:
1. For drivers to send their location into the system.
2. For riders to find nearby drivers given their position.

### Prerequisites/ Infrastructure requirements

* Java 7 or 8
* IntelliJ or any other IDE
* Apache installed on your machine. Apache ab is used in this project to do load testing.

## Built With

* [Java 8](http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) - the language used to run the application
Why is it used?
     - Java is one of the most popular programming languages used to create Web applications and platforms.
It is suitable to be used to develop an API due to its flexibility and excellent framework support.

* [Spring Boot](https://projects.spring.io/spring-boot/) - the framework used to run the application
Why is it used?
    - Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

* [Spring MVC](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html) - the framework used as a framework for the API
Why is it used?
     - Spring MVC provides a very clean division between controllers, JavaBean models, and views when used to build REST APIs.

* [H2 database](http://www.h2database.com/html/main.html) - the database used
Why is it used?
     - Very fast, open source, JDBC API
     - Embedded and server modes; in-memory databases
     - Browser based Console application
     - Small footprint: around 1.5 MB jar file size

* [Apache ab](http://jmeter.apache.org/) - the tool used to do load testing
Why is it used?
     - We can conveniently do load testing such as simulate number of threads accessing the API,
     and investigate response time.

### Setup instruction (Installing and running the application)

To run the API, please follow the steps below:
1. Go to src/main/java/Application and then run the main method. The API application then will be started.
You can do this through IDE or run in on the command line.
2. DRIVER_LOCATION table in the H2 database along with the seed data will be created.
Go to http://localhost:8080/h2 and then hit 'Connect' button. Inside this interface you will
be able to execute query to check against the data.
3. After the application running, you can run manual test/ unit test/ load testing to this API.
Please see the next section for instruction on how to run the test.
4. That's it :)

## Running the tests

### Unit testing

1. Before you can run the unit tests, you need to go to Application.java and run the API application from there.
2. Go to src/test/GoJekApiTesting for the unit test. There are two unit tests 'testSendLocationApi' and 'testFindDriversApi'. These two tests check against
invalid input (invalid driver id, latitude, longitude) as well as good valid input.

### Load testing

Apache ab is used to do load testing. http://httpd.apache.org/docs/current/programs/ab.html

You might want to increase your limit before start testing using Apache ab
```
ulimit -n 10000
```

The following is the result of **'Send Location API'** load testing after make 50,000 requests for 60 seconds as given per requirement.
Time per request: 81.285 [ms] (mean) which is still under our expected 100 ms response time.

You can find test data FindDriversSampleInput.json and SendLocSampleInput.json under 
/test_data folder on your downloaded file.

```
Rudys-MacBook-Pro:~ rudyrusli$ ab -k -c 400 -n 50000 -t 60 -u /Users/rudyrusli/Desktop/SendLocSampleInput.json -T 'application/json' http://localhost:8080/drivers/1/location
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 5000 requests
Completed 10000 requests
Completed 15000 requests
Completed 20000 requests
Completed 25000 requests
Completed 30000 requests
Completed 35000 requests
Completed 40000 requests
Completed 45000 requests
Completed 50000 requests
Finished 50000 requests


Server Software:        Apache-Coyote/1.1
Server Hostname:        localhost
Server Port:            8080

Document Path:          /drivers/1/location
Document Length:        2 bytes

Concurrency Level:      400
Time taken for tests:   10.161 seconds
Complete requests:      50000
Failed requests:        0
Keep-Alive requests:    49600
Total transferred:      10498000 bytes
Total body sent:        11800000
HTML transferred:       100000 bytes
Requests per second:    4920.96 [#/sec] (mean)
Time per request:       81.285 [ms] (mean)
Time per request:       0.203 [ms] (mean, across all concurrent requests)
Transfer rate:          1008.99 [Kbytes/sec] received
                        1134.13 kb/s sent
                        2143.12 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   8.4      0     157
Processing:     4   80  23.8     78     254
Waiting:        4   80  23.5     78     254
Total:          4   81  25.1     78     294

Percentage of the requests served within a certain time (ms)
  50%     78
  66%     82
  75%     86
  80%     88
  90%    102
  95%    137
  98%    156
  99%    168
 100%    294 (longest request)
```

The following is the result of **'Find Drivers API'** load testing after make 20 concurrent requests as given per expected-load-requirement.
Time per request: 13.360 [ms] (mean) which is still under our expected 100 ms response time.

```
Rudys-MacBook-Pro:~ rudyrusli$ ab -k -c 20 -n 60 -p /Users/rudyrusli/Desktop/FindDriversSampleInput.json -T 'application/json' http://localhost:8080/drivers/
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        Apache-Coyote/1.1
Server Hostname:        localhost
Server Port:            8080

Document Path:          /drivers/
Document Length:        0 bytes

Concurrency Level:      20
Time taken for tests:   0.040 seconds
Complete requests:      60
Failed requests:        0
Non-2xx responses:      60
Keep-Alive requests:    60
Total transferred:      11400 bytes
Total body sent:        14280
HTML transferred:       0 bytes
Requests per second:    1496.97 [#/sec] (mean)
Time per request:       13.360 [ms] (mean)
Time per request:       0.668 [ms] (mean, across all concurrent requests)
Transfer rate:          277.76 [Kbytes/sec] received
                        347.93 kb/s sent
                        625.69 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.6      0       2
Processing:     3   11   5.5      9      23
Waiting:        3   11   5.6      9      23
Total:          4   11   5.8      9      25

Percentage of the requests served within a certain time (ms)
  50%      9
  66%     11
  75%     15
  80%     16
  90%     20
  95%     24
  98%     25
  99%     25
 100%     25 (longest request)
```

## Deployment

Let's say we want to deploy this on a live system such as Heroku.

The steps will be the following:
In order to deploy to heroku after having your spring boot application,
you need to create a new heroku app

```
heroku create
Creating nameless-lake-8055 in organization heroku... done, stack is cedar-14
http://nameless-lake-8055.herokuapp.com/ | git@heroku.com:nameless-lake-8055.git
Git remote heroku added
```

* To deploy your code, execute 'git push heroku master'
* Heroku automatically detects the application as a Maven/Java app due to the presence of a pom.xml file.
* The application is now deployed.

For deployment for different environments, we can do something like this :

Let say we want to create both remote **staging** and **production** environment, then we
can start with staging first.


```
$heroku create --remote staging
Creating strong-river-216.... done
http://strong-river-216.heroku.com/ | https://git.heroku.com/strong-river-216.git
Git remote staging added
$git push staging master
...
$ heroku ps --remote staging
=== web: `bundle exec puma -C config/puma.rb``
web.1: up for 21s
```

Once staging is ready, then we create another one for production.

```
heroku create --remote production
Creating fierce-ice-327.... done
http://fierce-ice-327.heroku.com/ | https://git.heroku.com/fierce-ice-327.git
Git remote production added
$ git push production master
...
$ heroku ps --remote production
=== web: `bundle exec puma -C config/puma.rb``
web.1: up for 16s
```

Now we've got two same codebase running on different environments.

## Authors

* **Rudy Rusli** - [Rudy Rusli on LinkedIn](https://www.linkedin.com/in/rudyrusli/)