package com.uber_simulator;

import com.uber_simulator.service.UberService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created initial database and loads some seed data for us to use.
 */
@Component
public class DataLoader implements InitializingBean {
    @Autowired
    UberService uberService;

    /**
     * Prepare the database after the application is ready.
     *
     * @throws Exception In case something goes wrong.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        uberService.createDriverLocationTable();
        uberService.generateSeedData();
    }
}
