package com.uber_simulator.service;

import com.uber_simulator.beans.DriverLocationRequest;
import com.uber_simulator.beans.DriverLocationResponse;
import com.uber_simulator.dao.UberDao;
import com.uber_simulator.beans.DriverLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service layer to interact with driver's location data.
 */
@Service
public class UberService {

    @Autowired
    private UberDao uberDao;

    /**
     * Create DRIVER_LOCATION table.
     *
     * @return  number of updates to the database.
     */
    public int createDriverLocationTable(){
        return uberDao.createDriverLocationTable();
    }

    /**
     * Send driver's location.
     *
     * @param driverId  Id of driver who send its location.
     * @param driverLocation    Information about driver location.
     * @return number of updates to the database.
     */
    public int sendLocation(int driverId, DriverLocation driverLocation){
        return uberDao.sendLocation(driverId, driverLocation);
    }

    /**
     * Find nearby drivers.
     *
     * @param driverLocationRequest Request to find nearby drivers.
     * @return  list of nearby drivers.
     */
    public List<DriverLocationResponse> findDrivers(DriverLocationRequest driverLocationRequest) {
        return uberDao.findDrivers(driverLocationRequest);
    }

    /**
     * Generate some initial seed data for testing.
     */
    public void generateSeedData() {
        uberDao.generateSeedData();
    }
}

