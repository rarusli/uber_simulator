package com.uber_simulator.dao;

import com.uber_simulator.beans.DriverLocation;
import com.uber_simulator.beans.DriverLocationRequest;
import com.uber_simulator.beans.DriverLocationResponse;
import com.uber_simulator.dao.rowmapper.DriverLocationResponseMapper;
import com.uber_simulator.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Random;

/**
 * DAO layer to interact with driver's location data.
 */
@Repository
public class UberDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String DRIVER_LOCATION_TABLE = "DRIVER_LOCATION";

    /**
     * Create DRIVER_LOCATION table.
     *
     * @return  number of updates to the database.
     */
    public int createDriverLocationTable() {
        return jdbcTemplate.update(
        "CREATE TABLE IF NOT EXISTS " + DRIVER_LOCATION_TABLE + "(\n" +
             "  DRIVER_ID int primary key,\n" +
             "  LATITUDE DECIMAL(8,5),\n" +
             "  LONGITUDE DECIMAL(8,5),\n" +
             "  ACCURACY DECIMAL(2,1)\n" +
             ");");
    }

    /**
     * Send driver's location.
     *
     * @param driverId  Id of driver who send its location.
     * @param driverLoc Information about driver location.
     * @return number of updates to the database.
     */
    public int sendLocation(int driverId, DriverLocation driverLoc){
        String sql =
            "MERGE INTO " + DRIVER_LOCATION_TABLE + " KEY (DRIVER_ID) VALUES (?,?,?,?)";

        return jdbcTemplate.update(sql,
            driverId, driverLoc.getLatitude(), driverLoc.getLongitude(), driverLoc.getAccuracy());
    }

    /**
     * Find nearby drivers. We use 'Spherical Law of Cosines Formula' to find
     * coordinates within a certain radius.
     *
     * @param driverLocRequest Request to find nearby drivers.
     * @return  list of nearby drivers.
     */
    public List<DriverLocationResponse> findDrivers(DriverLocationRequest driverLocRequest) {
        String sql =
            "SELECT driver_id, latitude, longitude,\n" +
            "   ( ? * acos( cos( radians(?) ) * cos( radians( latitude ) )\n" +
            "   * cos( radians( longitude ) - radians(?) ) +\n" +
            "   sin( radians(?) ) * sin(radians(latitude)) ) ) AS distance\n" +
            "FROM " + DRIVER_LOCATION_TABLE + "\n" +
            "GROUP BY driver_id\n" +
            "HAVING distance < ?\n" +
            "ORDER BY distance\n" +
            "LIMIT ?";

        return jdbcTemplate.query(
            sql,
                new Object[] {
                    Constants.DISTANCE_FROM_EARTH_CENTER_TO_SURFACE,
                    driverLocRequest.getLatitude(), driverLocRequest.getLongitude(), driverLocRequest.getLatitude(),
                    driverLocRequest.getRadius(), driverLocRequest.getLimit()
                } , new DriverLocationResponseMapper());
    }

    /**
     * Generate some initial seed data for testing.
     */
    public void generateSeedData() {
        int id = 1;
        int count = 50000;
        System.out.println("Generating " + count + " initial seed data .. " + new java.util.Date());
        // The seed below will generate data around Jakarta, Indonesia
        while ( id <= count ) {
            Random r = new Random();
            float latitude = (float)((r.nextFloat() * -0.1) + -6.2);
            float longitude = (float)((r.nextFloat() * -0.1) + 106.84);
            float accuracy = r.nextFloat();

            // ** Uncomment below for debugging purpose.
            //System.out.println(latitude + ", " + longitude);

            DriverLocation driverLoc = new DriverLocation();
            driverLoc.setLatitude(latitude);
            driverLoc.setLongitude(longitude);
            driverLoc.setAccuracy((float)(Math.round(accuracy*10)/10.0));
            sendLocation(id, driverLoc);
            id++;
        }
        System.out.println("Done generating initial seed data .. " + new java.util.Date());
    }

} // end class


