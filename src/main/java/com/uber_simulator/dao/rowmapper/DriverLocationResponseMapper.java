package com.uber_simulator.dao.rowmapper;

import com.uber_simulator.beans.DriverLocationResponse;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Row mapper class to map ResultSet from database to a DriverLocationResponse object.
 */
public class DriverLocationResponseMapper implements RowMapper<DriverLocationResponse> {

    public DriverLocationResponse mapRow(ResultSet rs, int arg1) throws SQLException {
        DriverLocationResponse driverLocationResponse = new DriverLocationResponse();

        driverLocationResponse.setDriverId(rs.getInt("driver_id"));
        driverLocationResponse.setLatitude(rs.getFloat("latitude"));
        driverLocationResponse.setLongitude(rs.getFloat("longitude"));
        driverLocationResponse.setDistance(rs.getInt("distance"));

        return driverLocationResponse;
    }
}
