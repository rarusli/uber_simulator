package com.uber_simulator.thirrdparty;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

@Service
public class GoogleService {
    /** Endpoint for calculating distance between two coordinates (. */
    public static final String DISTANCE_CALCULATION_ENDPOINT = "http://google.com/distance";

    /** GSON to convert from Json to object and vice versa. */
    private Gson gson = new Gson();

    /** Http Client used to connect to our APIs. */
    private CloseableHttpClient httpClient = HttpClientBuilder.create().build();

    /**
     * Calculate distance between two latitude longitude using Google API.
     *
     * @param latitude1 First latitude
     * @param longitude1 First longitude
     * @param latitude2 Second latitude
     * @param longitude2 Second longitude
     * @return -1 if distance is not found, otherwise return distance.
     */
    public float calculateDistance(
            double latitude1, double longitude1, double latitude2, double longitude2) {

        float distance = -1;

        try {
            // Input checking for latitude and longitude.
            if ( !(isGoodLatitude(latitude1) && isGoodLatitude(latitude2) && isGoodLongitude(longitude1) &&
                    isGoodLongitude(longitude2)))
                return -1;

            HttpPut httpPut = new HttpPut(DISTANCE_CALCULATION_ENDPOINT);
            Location location = new Location();

            location.setLatitude1(latitude1);
            location.setLongitude1(longitude1);
            location.setLatitude2(latitude2);
            location.setLongitude2(longitude2);

            httpPut.setEntity(new StringEntity(gson.toJson(location), ContentType.APPLICATION_JSON));
            HttpResponse httpResponse = httpClient.execute(httpPut);
            int responseStatusCode = httpResponse.getStatusLine().getStatusCode();

            if ( responseStatusCode!= HttpStatus.OK.value() ) {
                return -1;
            }

            String responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

            distance = Float.valueOf(responseContent);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // close resources.

        }

        return distance;

    }

    private boolean isGoodLatitude(double latitude) {
        if ( latitude < -90 || latitude > 90 )
            return false;

        return true;
    }

    private boolean isGoodLongitude(double longitude) {
        if ( longitude < -180 || longitude > 180 )
            return false;

        return true;
    }

    /**
     * Return String given an InputStream.
     *
     * @param is    InputStream to be processed.
     * @return  String value of the InputStream.
     */
    private StringBuilder inputStreamToString(InputStream is) throws Exception {
        String line = "";
        StringBuilder total = new StringBuilder();

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) {
            total.append(line);
        }

        // Return full string
        return total;
    }


}

class Location {
    private double latitude1;
    private double latitude2;
    private double longitude1;
    private double longitude2;



    public double getLatitude1() {
        return latitude1;
    }

    public void setLatitude1(double latitude1) {
        this.latitude1 = latitude1;
    }

    public double getLatitude2() {
        return latitude2;
    }

    public void setLatitude2(double latitude2) {
        this.latitude2 = latitude2;
    }

    public double getLongitude1() {
        return longitude1;
    }

    public void setLongitude1(double longitude1) {
        this.longitude1 = longitude1;
    }

    public double getLongitude2() {
        return longitude2;
    }

    public void setLongitude2(double longitude2) {
        this.longitude2 = longitude2;
    }
}
