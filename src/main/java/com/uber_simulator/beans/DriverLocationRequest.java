package com.uber_simulator.beans;

/**
 * POJO to represent driver's location request to the API.
 */
public class DriverLocationRequest {

    private float latitude;

    private float longitude;

    // Default is 500 m
    private double radius = 0.5;

    // Limit is 10 entries
    private int limit = 10;

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "DriverLocationRequest{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", radius=" + radius +
                ", limit=" + limit +
                '}';
    }
}
