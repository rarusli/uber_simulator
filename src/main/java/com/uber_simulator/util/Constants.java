package com.uber_simulator.util;

/**
 * Class to save several static constant variables.
 */
public class Constants {

    /**
     * Valid driver id is from 1 - 50,000
     *
     * Used to validate user input to the APIs.
     */
    public static final int VALID_DRIVER_ID_LOWER_BOUND = 1;
    public static final int VALID_DRIVER_ID_HIGHER_BOUND = 50000;

    /**
     * Valid latitude is from -90 to 90.
     * Valid longitude is from -180 to 180.
     *
     * Used to validate user input to the APIs.
     */
    public static final double VALID_LAT_LOWER_BOUND = -90;
    public static final int VALID_LAT_UPPER_BOUND = 90;
    public static final int VALID_LON_LOWER_BOUND = -180;
    public static final int VALID_LON_UPPER_BOUND = 180;
    public static final String LAT_OUT_OF_BOUND_MSG =
            "{\"errors\":[\"Latitude should be between +/- 90\"]}";
    public static final String LON_OUT_OF_BOUND_MSG =
            "{\"errors\":[\"Longitude should be between +/- 180\"]}";
    public static final String EMPTY_JSON = "{}";

    /**
     * Distance from center of earth to its surface. This value is in km.
     *
     * Used for calculation to get nearby drivers.
     */
    public static final int DISTANCE_FROM_EARTH_CENTER_TO_SURFACE = 6371;


}
