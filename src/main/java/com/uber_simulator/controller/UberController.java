package com.uber_simulator.controller;

import com.uber_simulator.beans.DriverLocationRequest;
import com.uber_simulator.beans.DriverLocationResponse;
import com.uber_simulator.beans.DriverLocation;
import com.uber_simulator.service.UberService;
import com.uber_simulator.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for REST API.
 */
@RestController
public class UberController {

    @Autowired
    private UberService uberService;

    /**
     * REST API for drivers to send their location.
     *
     * @param id    Id of driver who send its location.
     * @param driverLocation    Information about driver location.
     * @return ok status with empty json body or error message if there's something wrong.
     */
    @RequestMapping(
        value="/drivers/{id}/location", method= RequestMethod.PUT, headers="Accept=application/json")
    public ResponseEntity<Object> sendLocation(
        @PathVariable(value="id") final int id, @RequestBody DriverLocation driverLocation)
    {
        // Some validation.
        if ( id < Constants.VALID_DRIVER_ID_LOWER_BOUND || id > Constants.VALID_DRIVER_ID_HIGHER_BOUND ) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Constants.EMPTY_JSON);
        } else if (driverLocation.getLatitude() < Constants.VALID_LAT_LOWER_BOUND ||
                driverLocation.getLatitude() > Constants.VALID_LAT_UPPER_BOUND) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(Constants.LAT_OUT_OF_BOUND_MSG);
        } else if (driverLocation.getLongitude() < Constants.VALID_LON_LOWER_BOUND ||
                driverLocation.getLongitude() > Constants.VALID_LON_UPPER_BOUND) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(Constants.LON_OUT_OF_BOUND_MSG);
        }

        // Send driver's location.
        uberService.sendLocation(id, driverLocation);
        return new ResponseEntity<>(Constants.EMPTY_JSON, HttpStatus.OK);
    }

    /**
     * REST API to find nearby drivers.
     *
     * @param driverLocationRequest Request to find nearby drivers.
     * @return  list of nearby drivers or error message if there's something wrong.
     */
    @RequestMapping(
        value="/drivers", method=RequestMethod.GET, headers="Accept=application/json")
    public ResponseEntity<Object> findDrivers(
            @RequestBody DriverLocationRequest driverLocationRequest)
    {
        // Some validation.
        if (driverLocationRequest.getLatitude() < Constants.VALID_LAT_LOWER_BOUND ||
                driverLocationRequest.getLatitude() > Constants.VALID_LAT_UPPER_BOUND) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Constants.LAT_OUT_OF_BOUND_MSG);
        } else if (driverLocationRequest.getLongitude() < Constants.VALID_LON_LOWER_BOUND ||
                driverLocationRequest.getLongitude() > Constants.VALID_LON_UPPER_BOUND) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Constants.LON_OUT_OF_BOUND_MSG);
        }

        // Find nearby drivers.
        List<DriverLocationResponse> driverLocations = uberService.findDrivers(driverLocationRequest);
        return new ResponseEntity(driverLocations, HttpStatus.OK);
    }


}