package com.uber_simulator.testing;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uber_simulator.beans.DriverLocation;
import com.uber_simulator.beans.DriverLocationRequest;
import com.uber_simulator.util.Constants;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit testing for the Uber Simulator API.
 *
 * Before you can run the unit tests, you need to go to Application.java and
 * run the application from there.
 */
public class UberSimulatorApiTesting {
    /** GSON to convert from Json to object and vice versa. */
    private Gson gson = new Gson();

    /** Http Client used to connect to our APIs. */
    private CloseableHttpClient httpClient = HttpClientBuilder.create().build();

    @Test
    public void testGoogleCalculateDistanceApi() {

        double latitude1 = -85.202575f;
        double longitude1 = 106.2;
        double latitude2 = -86.202575f;
        double longitude2 = 106.3;

        // Put unit test
        // dengan valid dan invalid input.
    }

    /**
     * Test 'Send Driver's Location API'.
     * This method will check against both good and invalid input.
     *
     * @throws IOException  if there's an exception occurs.
     */
    @Test
    public void testSendLocationApi() throws Exception {
        DriverLocation driverLocation = new DriverLocation();
        driverLocation.setAccuracy(0.9f);
        HttpPut httpPut = new HttpPut("http://localhost:8080/drivers/1/location");

        //////////////////////////////////////////////////
        // Check against invalid latitude
        //////////////////////////////////////////////////
        driverLocation.setLatitude(-91.202575f); // bad latitude
        driverLocation.setLongitude(106.846169f);
        httpPut.setEntity(new StringEntity(gson.toJson(driverLocation), ContentType.APPLICATION_JSON));
        HttpResponse httpResponse = httpClient.execute(httpPut);
        int responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        String responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

        // Check Http Status needs to be 422 and response content needs to display error message.
        assertEquals(responseStatusCode, HttpStatus.UNPROCESSABLE_ENTITY.value());
        assertTrue(responseContent.equals(Constants.LAT_OUT_OF_BOUND_MSG));

        //////////////////////////////////////////////////
        // Check against invalid longitude
        //////////////////////////////////////////////////
        driverLocation.setLatitude(-6.202575f);
        driverLocation.setLongitude(182.846169f); // bad longitude
        httpPut.setEntity(new StringEntity(gson.toJson(driverLocation), ContentType.APPLICATION_JSON));
        httpResponse = httpClient.execute(httpPut);
        responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

        // Check Http Status needs to be 422 and response content needs to display error message.
        assertEquals(responseStatusCode, HttpStatus.UNPROCESSABLE_ENTITY.value());
        assertTrue(responseContent.equals(Constants.LON_OUT_OF_BOUND_MSG));

        //////////////////////////////////////////////////
        // Check against invalid Driver Id.
        //////////////////////////////////////////////////
        driverLocation.setLatitude(-6.202575f);
        driverLocation.setLongitude(106.846169f);
        httpPut = new HttpPut("http://localhost:8080/drivers/0/location"); // invalid driver id
        httpPut.setEntity(new StringEntity(gson.toJson(driverLocation), ContentType.APPLICATION_JSON));
        httpResponse = httpClient.execute(httpPut);
        responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

        // Check Http Status needs to be 404 and response content needs to display error message.
        assertEquals(responseStatusCode, HttpStatus.NOT_FOUND.value());
        assertTrue(responseContent.equals(Constants.EMPTY_JSON));

        //////////////////////////////////////////////////
        // Check against good input.
        //////////////////////////////////////////////////
        driverLocation.setLatitude(-6.202575f);
        driverLocation.setLongitude(106.846169f);
        httpPut = new HttpPut("http://localhost:8080/drivers/1/location");
        httpPut.setEntity(new StringEntity(gson.toJson(driverLocation), ContentType.APPLICATION_JSON));
        httpResponse = httpClient.execute(httpPut);
        responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

        // Check Http Status and response content.
        assertEquals(responseStatusCode, HttpStatus.OK.value());
        assertTrue(responseContent.equals(Constants.EMPTY_JSON));
    }

    /**
     * Test 'Finding nearby drivers' API.
     * This method will check against both good and invalid input.
     *
     * @throws Exception    if there's an exception occurs.
     */
    @Test
    public void testFindingDriversApi() throws Exception {
        DriverLocationRequest driverLocRequest = new DriverLocationRequest();
        driverLocRequest.setRadius(10);
        driverLocRequest.setLimit(10);
        HttpGetWithEntity httpGet = new HttpGetWithEntity();
        httpGet.setURI(new URI("http://localhost:8080/drivers"));

        //////////////////////////////////////////////////
        // Check against invalid latitude
        //////////////////////////////////////////////////
        driverLocRequest.setLatitude(-91.202575f); // bad latitude
        driverLocRequest.setLongitude(106.846169f);
        httpGet.setEntity(new StringEntity(gson.toJson(driverLocRequest), ContentType.APPLICATION_JSON));
        HttpResponse httpResponse = httpClient.execute(httpGet);
        int responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        String responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

        // Check Http Status needs to be 400 and response content needs to display error message.
        assertEquals(responseStatusCode, HttpStatus.BAD_REQUEST.value());
        assertTrue(responseContent.equals(Constants.LAT_OUT_OF_BOUND_MSG));

        //////////////////////////////////////////////////
        // Check against invalid longitude
        //////////////////////////////////////////////////
        driverLocRequest.setLatitude(-6.202575f);
        driverLocRequest.setLongitude(182.846169f); // bad longitude
        httpGet.setEntity(new StringEntity(gson.toJson(driverLocRequest), ContentType.APPLICATION_JSON));
        httpResponse = httpClient.execute(httpGet);
        responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

        // Check Http Status needs to be 400 and response content needs to display error message.
        assertEquals(responseStatusCode, HttpStatus.BAD_REQUEST.value());
        assertTrue(responseContent.equals(Constants.LON_OUT_OF_BOUND_MSG));

        //////////////////////////////////////////////////
        // Check against good input
        //////////////////////////////////////////////////
        driverLocRequest.setLatitude(-6.202575f);
        driverLocRequest.setLongitude(106.846169f);
        httpGet.setEntity(new StringEntity(gson.toJson(driverLocRequest), ContentType.APPLICATION_JSON));
        httpResponse = httpClient.execute(httpGet);
        responseStatusCode = httpResponse.getStatusLine().getStatusCode();
        responseContent = inputStreamToString(httpResponse.getEntity().getContent()).toString();

        // Check Http Status needs to be 200.
        assertEquals(responseStatusCode, HttpStatus.OK.value());

        // *** Uncomment below if want to display output.
        /** /
        List<LinkedHashMap<String,String>> driverLocations =
                retrieveResourceFromResponse(httpResponse, java.util.List.class);

        for ( int i = 0; i < driverLocations.size(); i++ ) {
            LinkedHashMap<String,String> map = driverLocations.get(i);

            for ( Map.Entry<String,String> entries : map.entrySet() ) {
                String key = entries.getKey();
                String value = String.valueOf(entries.getValue());

                System.out.println("key:" + key + ", value:" + value);
            }
        }/**/
    }

    /**
     * Return String given an InputStream.
     *
     * @param is    InputStream to be processed.
     * @return  String value of the InputStream.
     */
    private StringBuilder inputStreamToString(InputStream is) throws Exception {
        String line = "";
        StringBuilder total = new StringBuilder();

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) {
            total.append(line);
        }

        // Return full string
        return total;
    }

    /**
     *  Method to retrieve resource from response.
     * @param response
     * @param clazz
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T retrieveResourceFromResponse(HttpResponse response, Class<T> clazz)
            throws IOException {

        String jsonFromResponse = EntityUtils.toString(response.getEntity());
        ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(jsonFromResponse, clazz);
    }
}