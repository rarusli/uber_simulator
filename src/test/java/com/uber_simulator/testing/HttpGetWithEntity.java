package com.uber_simulator.testing;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

/**
 * Class used so that we can send http get with entity in our unit test.
 */
public class HttpGetWithEntity extends HttpEntityEnclosingRequestBase {
    public final static String METHOD_NAME = "GET";


    @Override
    public String getMethod() {
        return METHOD_NAME;
    }
}
